# Open Mobility indicator Jupyter notebook Différence de trafic routier annuel à Montpellier Métropole

## description : [indicator.yaml file](https://gitlab.com/open-mobility-indicators/indicators/3M_diff_trafic/-/blob/main/indicator.yaml)

Ce notebook affiche les différence de trafic dans les moyennes annuelles de débit (MJA) 
calculée à partir du tableau CSV produit par le notebook [comptages_3M](https://gitlab.com/open-mobility-indicators/indicators/comptages_3M) à partir de [l'open data 3M](https://data.montpellier3m.fr/dataset/comptage-vehicules-particuliers-de-montpellier/resource/2002371e-b66a-46a8-a715-d6a93ea72f26).

forked from [jupyter notebook template](https://gitlab.com/open-mobility-indicators/indicator-templates/jupyter-notebook)

[Install locally using a venv or Docker, Build and Use (download, compute) : see the wiki doc.](https://gitlab.com/open-mobility-indicators/website/-/wikis/2_contributeur_technique/install-a-notebook-locally)  
